<?php

/**
 * @author Kelompok STBI
 * @copyright 2015
 */

class Crawler
{

    private $base_url;
    private $pivot_url;
    private $max_deep;
    private $in_host;
    private $visited_urls = [];

    public function __construct($pivot_url, $max_deep = 0, $in_host = true)
    {
        require_once ('rollingcurlx/rollingcurlx.class.php');
        
        $this->options = array(
            CURLOPT_HEADER => true,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_ENCODING => 'gzip, deflate, sdch',
            CURLOPT_RETURNTRANSFER => true);
        
        $this->headers = array('Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Connection:keep-alive');
                
        $this->max_deep = $max_deep;
        $this->in_host = $in_host;
        $this->pivot_url = $pivot_url;
        $this->base_url = 'http://' . parse_url($pivot_url, PHP_URL_HOST);

        libxml_use_internal_errors(true);
    }

    /*
     * method untuk memulai mengeksekusi
     */
    public function execute()
    {
        $this->fetchHtml([$this->pivot_url]);
    }
    
    /*
     * method untuk mengubah url untuk dijadikan nama file
     */
    private function urlName($url) {
        return preg_replace('/[^a-zA-Z0-9]/', '_', $url);
    }
    
    /*
     * method untuk mengambil dokumen berdasarkan link
     */
    private function fetchHtml($urls, $deep = 0)
    {
        $curl = new RollingCurlX(30);
        
        // proses fetching halaman dilakukan secara paralel / multi curl   
        foreach ($urls as $url)
        {
            if(!array_key_exists($url, $this->visited_urls)) // jika link belum pernah difetch
            {
                echo 'VISITING :: ' . $url . '<br>';
                $this->visited_urls[$url] = $deep; // catat link kedalam array
                $data['deep'] = $deep;
                $curl->addRequest($url, NULL, function ($response, $url, $info, $data, $time)
                {
                    if ($info['http_code'] == 200)
                    {
                        $this->saveToFile($url, $response); // simpan hasil dokumen
                        if ($data['deep'] < $this->max_deep) // cek apakah deep kurang dari batas
                        {
                            $links = $this->getLinks($response, $this->in_host); // memparsing link dari dokumen
                            $this->fetchHtml($links, $data['deep']++); // memfetch kembali link tersebut / rekursif
                        }
                    }
                }, $data, $this->options, $this->headers);
            } else { // jika link sudah pernah dikunjungi
                echo 'ABORTING :: ' . $url . '<br>';
            }
        }

        $curl->setTimeout(300000);
        $curl->execute();
    }
    
    /*
     * method untuk memparsing link yang ada dalam dokumen
     */
    private function getLinks($html, $in_host = true)
    {
        $dom = new DomDocument;
        $dom->loadHTML($html);
        $xpath = new DOMXPath($dom);
        
        // pilihan jika link yang dikunjungi hanya dari host yang sama atau sembarang link
        $query = ($in_host) ? "a[contains(@href, '$this->base_url') or starts-with(@href, '/')]/@href" : "a/@href";

        $nodes = $xpath->query("//$query");

        $result = [];
        foreach ($nodes as $node)
        {
            $href = $node->nodeValue;
            if(preg_match('/^\/[a-zA-Z](.*)/', $href)) { // jika merupakan relatif url, e.g : href="/about"
                $href = $this->base_url . $href;
            }
            $result[] = $href;
        }

        return $result;
    }
    
    /*
     * method untuk menyimpan hasil fetching ke dalam file txt
     */
    private function saveToFile($url, $content) 
    {
        $target_path = dirname(__FILE__).'/../outputs/'.$this->urlName($url).'.txt';
        $fh = fopen($target_path, 'w');
        fwrite($fh, $content);
        fclose($fh);
    }
    
}

?>